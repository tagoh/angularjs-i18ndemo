#! /usr/bin/env node

var path = require('path');
var exec = require('child_process').exec;

var home = process.env.HOME;
var newhome = path.join(home,'app-root', 'runtime');
var epath = process.env.PATH;
console.log('Set up $HOME to ' + newhome);

var cmd = 'node node_modules/bower/bin/bower install';
console.log('Exec ' + cmd);
exec(cmd, {env: {'HOME': newhome, 'PATH': epath}}, function(err, stdout, stderr) {
  console.log('stdout: ' + stdout);
  console.log('stderr: ' + stderr);
  if (err != null) {
    console.log('error: ' + err);
  }
});
