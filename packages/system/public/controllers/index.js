'use strict';

angular.module('mean.system')
    .controller('IndexController',
		['$scope', 'Global', 'gettextCatalog',
		 function($scope, Global, gettextCatalog) {
		     $scope.global = Global;
		     $scope.languages = {
			 current: gettextCatalog.currentLanguage,
			 available: {
			     'ja': 'Japanese',
			     'en': 'English'
			 }
		     };
		     $scope.$watch('languages.current', function(lang) {
			 if (!lang) {
			     return;
			 }
			 gettextCatalog.setCurrentLanguage(lang);
			 if (lang !== 'en')
			     gettextCatalog.loadRemote('/languages/' + lang + '.json');
		     });
		 }
		]);
